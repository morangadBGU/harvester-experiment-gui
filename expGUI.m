function varargout = expGUI(varargin)
% EXPGUI MATLAB code for expGUI.fig
%      EXPGUI, by itself, creates a new EXPGUI or raises the existing
%      singleton*.
%
%      H = EXPGUI returns the handle to a new EXPGUI or the handle to
%      the existing singleton*.
%
%      EXPGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXPGUI.M with the given input arguments.
%
%      EXPGUI('Property','Value',...) creates a new EXPGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before expGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to expGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help expGUI

% Last Modified by GUIDE v2.5 19-May-2015 08:30:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @expGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @expGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before expGUI is made visible.
function expGUI_OpeningFcn(hObject, eventdata, handles, varargin)
clc;

instrreset();



% Choose default command line output for expGUI
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
load('Opti_curr_2015.mat'); 
setGUIparams();
setGlobalParams();

% --- Outputs from this function are returned to the command line.
function varargout = expGUI_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function LeftAxes_CreateFcn(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in Kselector.
function Kselector_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function Kselector_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in left.
function left_Callback(hObject, eventdata, handles)
global offset
offset=offset-1;
UpdatProfile();
    
% --- Executes on button press in right.
function right_Callback(hObject, eventdata, handles)
global offset
offset=offset+1;
UpdatProfile();

% --- Executes on button press in pause.
function pause_Callback(hObject, eventdata, handles)
global pause;
pause=get(hObject,'Value');

 if (pause)
     set(handles.notes,'String','Pause');
     set(handles.Record,'Value',0);
 else
     set(handles.notes,'String','Play');
 end

% --- Executes during object creation, after setting all properties.
function PlotSelector_CreateFcn(hObject, eventdata, handles)

% --- Executes on button press in CalcHpower.
function CalcHpower_Callback(hObject, eventdata, handles)
global CalcHpower
CalcHpower=get(hObject,'Value');

% --- Executes on button press in CalcInertia.
function CalcInertia_Callback(hObject, eventdata, handles)

% --- Executes on button press in CalcVel.
function CalcVel_Callback(hObject, eventdata, handles)
global CalcVel
CalcVel=get(hObject,'Value');

% --- Executes on button press in CalcDataON.
function CalcDataON_Callback(hObject, eventdata, handles)
global CalcData
CalcData=get(hObject,'Value');

% --- Executes on button press in TheoDataON.
function TheoDataON_Callback(hObject, eventdata, handles)
global TheoDataON
TheoDataON=get(hObject,'Value');

% --- Executes during object creation, after setting all properties.
function HarvestON_CreateFcn(hObject, eventdata, handles)

function Name_Callback(hObject, eventdata, handles)
global FileName
<<<<<<< HEAD
n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=num2str(PR(n,:));
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_' ...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String') ...
    ,'_',PR,'_',date,'_0'];
=======
FileName=updateFileName(handles);
>>>>>>> origin/master

% --- Executes during object creation, after setting all properties.
function Name_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Hight_Callback(hObject, eventdata, handles)
global FileName
<<<<<<< HEAD
n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=num2str(PR(n,:));
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_' ...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String') ...
    ,'_',PR,'_',date,'_0'];
=======
FileName=updateFileName(handles);
>>>>>>> origin/master

% --- Executes during object creation, after setting all properties.
function Hight_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Weight_Callback(hObject, eventdata, handles)
global FileName 
<<<<<<< HEAD
n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=num2str(PR(n,:));
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_' ...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String') ...
    ,'_',PR,'_',date,'_0'];UpdatProfile();
=======
FileName=updateFileName(handles);
UpdatProfile();
>>>>>>> origin/master



% --- Executes during object creation, after setting all properties.
function Weight_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Age_Callback(hObject, eventdata, handles)
global FileName
<<<<<<< HEAD
n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=num2str(PR(n,:));
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_' ...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String') ...
    ,'_',PR,'_',date,'_0'];
=======
FileName=updateFileName(handles);

>>>>>>> origin/master
% --- Executes during object creation, after setting all properties.
function Age_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in Ang.
function Ang_Callback(hObject, eventdata, handles)

% --- Executes on button press in Vin.
function Vin_Callback(hObject, eventdata, handles)

% --- Executes on button press in Vout.
function Vout_Callback(hObject, eventdata, handles)

% % --- Executes on button press in PR.
% function HarvestTorque_Callback(hObject, eventdata, handles)
% global Tk
% n=get(handle.HarvestTorque,'Value');
% tmp=get(handle.HarvestTorque,'String'); tmp=tmp(n,1:end-1);
% PR=str2double(tmp);
% for k=1:4
%     Tk{k}=Tk{k}*PR;
% end

% --- Executes on button press in AngFilt.
function AngFilt_Callback(hObject, eventdata, handles)

% --- Executes on button press in VinFilt.
function VinFilt_Callback(hObject, eventdata, handles)

% --- Executes on button press in VoutFilt.
function VoutFilt_Callback(hObject, eventdata, handles)

% --- Executes on button press in HarvestTorqueFilt.
function HarvestTorqueFilt_Callback(hObject, eventdata, handles)

% --- Executes on button press in TorqueMeter.
function TorqueMeter_Callback(hObject, eventdata, handles)

% --- Executes on button press in FiltTorqueMeter.
function FiltTorqueMeter_Callback(hObject, eventdata, handles)

% --- Executes on button press in Record.
function Record_Callback(hObject, eventdata, handles)
global Record RecData FileName RecCounter
Record=get(hObject,'Value');
if(Record)
    set(handles .notes,'String','Recording...','ForegroundColor',[1 0 0]);
    RecCounter=zeros(2,1);
    RecData{1}=zeros(5*60*200,8); % max record time of 5 minutes. (Left)
    RecData{2}=zeros(5*60*200,8); % max record time of 5 minutes. (Right)
else
    set(handles.notes,'String','Recording Stoped!','ForegroundColor',[0 0 0]);
    set(handles.pause,'value',1); % pausing plot while saving
    set(handles.notes,'String','Saving!','ForegroundColor',[0 0 0]);
    FileName=updateFileName(handles);
    if(get(handles.EnableLeft,'Value'))
        SaveData=RecData{1}(1:RecCounter(1),:);
        save(['Data\',FileName,'_Left.mat'],'SaveData');
    end
    if(get(handles.EnableRight,'Value'))
        SaveData=RecData{2}(1:RecCounter(2),:);
        save(['Data\',FileName,'_Right.mat'],'SaveData');
    end
    set(handles.pause,'value',0); %unpausing plot
    set(handles.notes,'String',['Data Saved to: ',FileName,'.mat'],'ForegroundColor',[0 0 0]);
    
end
%     RecCount=RecCount+1;
%     FileName(end)='';
%     FileName=[FileName,num2str(RecCount)];
%     set(handles.pause,'Value',0);
% end


% --- Executes on button press in OpenL.
function OpenL_Callback(hObject, eventdata, handles)
    global S LeftPort 
    hand=[handles.SerialPortS1,handles.SerialPortS2];
    ii=1;
    n=get(hand(ii),'Value');
    tmp=get(hand(ii),'String');
    LeftPort=cell2mat(tmp(n));
    OpenSScript();
    
    

% --- Executes on button press in CloseL.
function CloseL_Callback(hObject, eventdata, handles)
    global S DataVect
    ii=1;
    CloseSScript();
    cla(handles.LeftAxes);
    cla(handles.RightAxes);
       
% --- Executes on button press in OpenSR.
function OpenSR_Callback(hObject, eventdata, handles)
    global S 
    hand=[handles.SerialPortS1,handles.SerialPortS2];
    ii=2; 
    OpenSScript();
    

% --- Executes on button press in closeSR.
function closeSR_Callback(hObject, eventdata, handles)
    global S;
    ii=2;
    CloseSScript();
    cla(handles.LeftAxes);
    cla(handles.RightAxes);
    

% --- Executes on button press in TheoNeg.
function TheoNeg_Callback(hObject, eventdata, handles)
global TheoNegON
TheoNegON=get(hObject,'Value');

% --- Executes on selection change in SerialPortS1.
function SerialPortS1_Callback(hObject, eventdata, handles)
% hObject    handle to SerialPortS1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns SerialPortS1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from SerialPortS1

% --- Executes during object creation, after setting all properties.
function SerialPortS1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SerialPortS1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function SendToCont_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SendToCont (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in SaveFig.
function SaveFig_Callback(hObject, eventdata, handles)
global FileName FigCount
FigCount=FigCount+1;
% FigFileName=horzcat(FileName,'_',num2str(FigCount),'.fig');
% savefig(FigFileName);
FileName=updateFileName(handles);
FigFileName=horzcat(FileName,'_',num2str(FigCount));
saveas(handles.LeftAxes,['Data\', FigFileName],'fig')
set(handles.notes,'String',['Figure Saved to: ',FigFileName,'.fig'],'ForegroundColor',[0 0 0]);


% --- Executes on selection change in SerialPortS2.
function SerialPortS2_Callback(hObject, eventdata, handles)
% hObject    handle to SerialPortS2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns SerialPortS2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from SerialPortS2


% --- Executes during object creation, after setting all properties.
function SerialPortS2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SerialPortS2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Resistor_Callback(hObject, eventdata, handles)
global Resistor FileName
Resistor=str2num(get(handles.Resistor,'string'));
FileName=updateFileName(handles);

% --- Executes during object creation, after setting all properties.
function Resistor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Resistor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- takes hall angle to Zero
function ZeroHall_Callback(hObject, eventdata, handles)
global HallBiasflag
HallBiasflag=1;



function PR_Callback(hObject, eventdata, handles)
global FileName
UpdatProfile();
<<<<<<< HEAD
n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=num2str(PR(n,:));
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_' ...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String') ...
    ,'_',PR,'_',date,'_0'];

=======
FileName=updateFileName(handles);
>>>>>>> origin/master

% --- Executes during object creation, after setting all properties.
function PR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function K1UpdateTime_Callback(hObject, eventdata, handles)
% hObject    handle to K1UpdateTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of K1UpdateTime as text
%        str2double(get(hObject,'String')) returns contents of K1UpdateTime as a double

% --- Executes during object creation, after setting all properties.
function K1UpdateTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to K1UpdateTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function K3UpdateTime_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function K3UpdateTime_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function K4UpdateTime_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function K4UpdateTime_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function EnableLeft_Callback(hObject, eventdata, handles)

function EnableRight_Callback(hObject, eventdata, handles)


% --- Executes when selected object is changed in Control.
function Control_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in Control 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in SendProfile.
function SendProfile_Callback(hObject, eventdata, handles)
global S Tk
% Tk{1}=Tk{1}*0;
CurrentProfiles=[Tk{1} Tk{3} -Tk{4}]*25+500;
CurrentProfiles(CurrentProfiles<500)=500;
DataOut=typecast(uint16(CurrentProfiles),'uint8');
if(get(handles.EnableLeft,'value'))    
    fwrite(S{1},['!','!'],'uint8'); pause(2);
    fwrite(S{1},DataOut,'uint8');
    cla(handles.LeftAxes);
    set(handles.notes,'String','Left Controler Profile Sent');
    pause(1);
    set(handles.notes,'String','...');
end
if(get(handles.EnableRight,'value'))
    fwrite(S{2},['!','!'],'uint8'); pause(2);
    fwrite(S{2},DataOut,'uint8');
    cla(handles.RightAxes);
    set(handles.notes,'String','Right Controler Profile Sent');
    pause(1);
    set(handles.notes,'String','...');
end


% --- Executes on button press in ResetKneePos.
function ResetKneePos_Callback(hObject, eventdata, handles)
global S
if(get(handles.EnableLeft,'value'))    
    fwrite(S{1},['@','@'],'uint8'); pause(2);    
%     cla(handles.LeftAxes);
end
if(get(handles.EnableRight,'value'))
    fwrite(S{2},['@','@'],'uint8'); pause(2);
%     cla(handles.RightAxes);
end
if(get(handles.ResetKneePos,'Value')==1)
    set(handles.ResetKneePos,'Value',0)
end


% --- Executes on selection change in PR.
function popupmenu5_Callback(hObject, eventdata, handles)
% hObject    handle to PR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PR contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PR


% --- Executes during object creation, after setting all properties.
function HarvestTorque_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bLeft_Callback(hObject, eventdata, handles)
global b a
b.Left=eval(get(handles.bLeft,'string'));
save('angleCalib.mat','a','b');

% --- Executes during object creation, after setting all properties.
function bLeft_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function aLeft_Callback(hObject, eventdata, handles)
global a b
a.Left=eval(get(handles.aLeft,'string'));
save('angleCalib.mat','a','b');


% --- Executes during object creation, after setting all properties.
function aLeft_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bRight_Callback(hObject, eventdata, handles)
global b a
b.Right=eval(get(handles.bRight,'string'));
save('angleCalib.mat','a','b');


% --- Executes during object creation, after setting all properties.
function bRight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function aRight_Callback(hObject, eventdata, handles)
global a b
a.Right=eval(get(handles.aRight,'string'));
save('angleCalib.mat','a','b');


% --- Executes during object creation, after setting all properties.
function aRight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to aRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
