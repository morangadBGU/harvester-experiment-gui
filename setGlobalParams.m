
setHarvestReferance1();
% Serial Port
global PacketSize Counter
PacketSize=21; Counter=zeros(2,1);

%% Handles
global ghandles
ghandles=handles;
%% General 
global dt GR Ig C_const GR2;
dt=1/SampleRate;
GR=243; GR2=GR^2;              %Gear ratio (bavel gear-1:3, planetar gear-1:81)
Ig_supp=0.248;                 %planetar gear, moment of inertia [gcm^2]
Ig=(5.54+Ig_supp)*1e-7;        %Total moment of inertia, engine and planetar gear [Kg-m^2]
C_const=0.3107;                %C parameter, viscous velocity parameter
%% Plot Axes

hold(handles.LeftAxes,'off'); hold(handles.RightAxes,'off'); 
grid(handles.LeftAxes,'on'); grid(handles.RightAxes,'on');
% global time Pot Hall %plot buffers
% time=zeros(1,200*3); Pot=zeros(1,200*3); Hall=zeros(1,200*3);
%% serials
global S
S=cell(2,1);
%% Data structure
% global  Data %DataVect 
Data.Pot=0;
Data.Hall=0;
Data.HarvestT=0;
Data.Vin=0;
Data.Vout=0;
Data.IndexInSeg=0;          % in current segment
Data.SegAVGlength=0;        % off current segment  
Data.K=0;
Data.time=0;
Data.SegPR=0;
Data.Iref=0;
Data.SegUpdate=0;

% DataVect=[Data Data Data;Data Data Data]; %contains 3 last samples for each side (L/R)
%% LPFs
global PotFiltFunc HallFiltFunc HarvestTFiltFunc
a=[0 0 0]; b=[1 0 0]; %coefs for pot filter
PotFiltFunc =@(RawVect,FiltVect) b*RawVect-a(2:end)*FiltVect;
a=[0 0 0]; b=[1 0 0]; %coefs for Hall filter
HallFiltFunc =@(RawVect,FiltVect) b*RawVect-a(2:end)*FiltVect;
a=[0 0 0]; b=[1 0 0]; %coefs for HarvestT filter
HarvestTFiltFunc =@(RawVect,FiltVect) b*RawVect-a(2:end)*FiltVect;

%% TorquMeter
global TorqueMeterFunc
a=1.218785171275625; b=-0.372872248130212;
TorqueMeterFunc=@(d_theta,omegaHall) a*d_theta+b*omegaHall; % Fitted model of TorqueMeter

%% Hall bias
global HallBias HallBiasflag
HallBias=0; HallBiasflag=0;

%% Recording
global FileName RecCounter RecData FigCount
 n=get(handles.PR,'Value');
tmp=get(handles.PR,'String'); tmp=tmp(n,1:end-1);
PR=0.01*str2double(tmp);
FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_'...
<<<<<<< HEAD
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String')...
    ,'_',num2str(PR),'%_',date,'_0'];
=======
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String'),'_',get(handles.Resistor,'String')...
    ,'_',num2str(PR),'%_',datestr(now,'dd_mm_yy_HH_MM_SS')];
>>>>>>> origin/master
RecCounter=zeros(2,1);
RecData=cell(2,1); 
RecData{1}=zeros(5*60*200,8); % max record time of 5 minutes. (Left)
RecData{2}=zeros(5*60*200,8); % max record time of 5 minutes. (Right)
Saving=0;
FigCount=0; 

%% Print Buffer
global PlotBuffFull PlotBuffIndex PlotBuffer PlotBuffLength
PlotBuffFull=zeros(2,1); PlotBuffLength=250;
PlotBuffIndex=zeros(2,1);
PlotBuffer=cell(2,1); 
PlotBuffer{1}=zeros(PlotBuffLength,9); PlotBuffer{2}=zeros(PlotBuffLength,9);

%% profile updating 
global ProfileVersion
ProfileVersion=0;

%% angle calibration
global a b
a.Right=str2double(get(handles.aRight,'string'));
b.Right=str2double(get(handles.bRight,'string'));
a.Left=str2double(get(handles.aLeft,'string'));
b.Left=str2double(get(handles.bLeft,'string'));
clear a; clear b;