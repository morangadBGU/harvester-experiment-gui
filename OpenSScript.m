global PacketSize %StTime 
global fTest timeOld
timeOld=0;
n=get(hand(ii),'Value');
tmp=get(hand(ii),'String');
Port=cell2mat(tmp(n));
if (n>1)&&(~strcmp(class(S{ii}),'serial'))    
    S{ii}=serial(Port);
    S{ii}.BaudRate=115200;      
    S{ii}.BytesAvailableFcnCount = PacketSize;
    S{ii}.BytesAvailableFcnMode = 'byte';
    S{ii}.OutputBufferSize=1024;
%     S{ii}.BytesAvailableFcnMode = 'terminator';
    S{ii}.BytesAvailableFcn = @DataIn;

end
if (n>1)&&(strcmp(S{ii}.status,'closed'))
    set(handles.notes,'String',['Opening Serial Port ',Port,'...'],'ForegroundColor',[0 0 0]);
    drawnow();
    fTest=tic();
    fopen(S{ii});
    set(handles.notes,'String',['Serial Port ',Port,' open'],'ForegroundColor',[0 0 0]);
    if(ii==1)
        set(handles.EnableLeft,'value',1);
    else
        set(handles.EnableRight,'value',1);
    end
else
    if(n==1)
        set(handles.notes,'String',['Select a Serial Port!'],'ForegroundColor',[1 0 0]);        
    else
        set(handles.notes,'String',['Serial Port ',Port,' already open!'],'ForegroundColor',[1 0 0]);    
    end

end