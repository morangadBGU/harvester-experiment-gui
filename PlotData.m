plot(axesHandle,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,2),'k' ...       % Angle (potentiometer)
    ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,3),'--k' ...                % 0% to 100% of a gait
    ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,4),'r' ...                  % Harvest Torque
    ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,5),'b' ...                  % V in
	,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,6),'g' ...                  % V out
    ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,7)*10,'c' ...               % Segment Identification
	,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,8),'--m' ...                % I ref
    ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,9)*10,'--r','linewidth',2); 

% plot(axesHandle,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,2),'k' ...    
%     ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,4),'r' ...    
%     ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,5),'b' ...
% 	,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,6),'g' ...
%     ,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,7)*10,'c' ...
% 	,PlotBuffer{SideNum}(:,1),PlotBuffer{SideNum}(:,8),'m','linewidth',2); 
grid(axesHandle,'on');
drawnow();

% xlim(axesHandle,[PlotBuffer(1,1)-8 PlotBuffer(1,1)]);
% if(ClearPlots) %clears plot every 1000 data points
% %     cla(axesHandle);
%     plotCount=0;
%     ClearPlots=0;
% end




