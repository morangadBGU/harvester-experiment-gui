time=[time(1:end-1) DataVect(SideNum,2).time];
Pot=[Pot(1:end-1) DataVect(SideNum,2).Pot];
Hall=[Hall(1:end-1) DataVect(SideNum,2).Hall];

plot(axesHandle,time,Pot,'k',time,Hall,'--k','linwidth',2);

% plot(axesHandle,DataVect(SideNum,2).time,DataVect(SideNum,2).Pot,'.k');
% plot(axesHandle,DataVect(SideNum,2).time ,DataVect(SideNum,2).Hall ,'.b');
% % plot(axesHandle,[DataVect(SideNum,3).time DataVect(SideNum,2).time],[DataVect(SideNum,3).HarvestT.Raw DataVect(SideNum,2).HarvestT.Raw],'r','linewidth',2);
% % plot(axesHandle,[DataVect(SideNum,3).time DataVect(SideNum,2).time],[DataVect(SideNum,3).Vin DataVect(SideNum,2).Vin],'b','linewidth',2);
% % plot(axesHandle,[DataVect(SideNum,3).time DataVect(SideNum,2).time],[DataVect(SideNum,3).Vout DataVect(SideNum,2).Vout],'g','linewidth',2);
% plot(axesHandle,DataVect(SideNum,2).time,DataVect(SideNum,2).K*10,'.g');

xlim(axesHandle,[DataVect(SideNum,2).time-10 DataVect(SideNum,2).time+10]);
if(~mod(Counter(SideNum),1000))
    cla(axesHandle);
end
drawnow();



