function FileName=updateFileName(handles)
% update the file name

n=get(handles.PR,'value');
PR=get(handles.PR,'string'); PR=cell2mat(PR(n,:));

FileName=[get(handles.Name,'String'),'_',get(handles.Hight,'String'),'_'...
    ,get(handles.Weight,'String'),'_',get(handles.Age,'String'),'_',get(handles.Resistor,'String')...
    ,'_',PR,'_',datestr(now,'dd_mm_yy_HH_MM_SS')];

end