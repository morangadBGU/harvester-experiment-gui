function [ intOut ] = Bytes2Int16( bytesArr )

a=bitshift(bytesArr(1),8);  %add 8 zeros on the right (exmple: 10101010 --> 1010101000000000)
b=bytesArr(2);

intOut=bitor(a,b); % unified a and b to the original value
if(intOut>2^15)    % check if the original value was negative
    intOut=intOut-2^16;
end
intOut=double(typecast(int32(intOut),'int32'));  % convert the value to 32 bit (double)
end

