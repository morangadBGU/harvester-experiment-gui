global Tk TkSource offset
offset=0;
TkSource=cell(4,5);
Tk=cell(4,1);

K1=TorqueProfile{1,1}; K3=TorqueProfile{1,2}; K4=TorqueProfile{1,3};

%% splining and saving NEG torque profiles 
for n=1:6
    TkSource{1,n}=interp1(1:length(K1(:,n)),K1(:,n),linspace(1,length(K1),100)); % interp K1 Torque profile to 100 points
    TkSource{2,n}=zeros(1,100);
    TkSource{3,n}=interp1(1:length(K3(:,n)),K3(:,n),linspace(1,length(K3),100)); % interp K3 Torque profile to 100 points
    TkSource{4,n}=interp1(1:length(K4(:,n)),K4(:,n),linspace(1,length(K4),100)); % interp K4 Torque profile to 100 points
end

SetProfileAsAngFunction();

UpdatProfile();
%%
% Tk{1}=zeros(1,100);%TkSource{1}*str2double(get(handles.Weight,'string'));
% Tk{2}=zeros(1,100); % no harvest on K2
% Tk{3}=TkSource{3}/80*str2double(get(handles.Weight,'string'));
% Tk{4}=TkSource{4}/80*str2double(get(handles.Weight,'string'));

% figure();
% subplot(4,1,1); plot(TkSource{1}); 
% subplot(4,1,2); plot(TkSource{2});
% subplot(4,1,3); plot(TkSource{3});
% subplot(4,1,4); plot(TkSource{4});