global Tk TkSource offset
PR=get(handles.PR,'Value');
% tmp=get(handles.PR,'String'); tmp=tmp(n,1:end-1);
% PR=0.01*str2double(tmp);
K=get(handles.Kselector,'Value');
if(K==5)
     for K=[1,3,4]
        if(offset<0)
            Toffset=[TkSource{K,PR}(1-offset:end) zeros(1,-offset)];
        else
            Toffset=[zeros(1,offset) TkSource{K,PR}(1:end-offset)];
        end
        Tk{K,1}=Toffset*str2double(get(handles.Weight,'string'));              
        plotProf();
     end    
else
    if(K~=2)
        if(offset<0)
            Toffset=[TkSource{K,PR}(1-offset:end) zeros(1,-offset)];
        else
            Toffset=[zeros(1,offset) TkSource{K,PR}(1:end-offset)];
        end
        Tk{K,1}=Toffset*str2double(get(handles.Weight,'string'))*PR;    
        plotProf(); 
    end
end