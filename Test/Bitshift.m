a = intmax('uint8');
s1 = 'Initial uint8 value %5d is %08s in binary\n';
s2 = 'Shifted uint8 value %5d is %08s in binary\n';
fprintf(s1,a,dec2bin(a))
 for i = 1:8
    a = bitshift(a,-1);
    fprintf(s2,a,dec2bin(a))
 end