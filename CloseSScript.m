set(handles.notes,'String','Closing Serial Port...');
drawnow();
if(strcmp(class(S{ii}),'serial'))&&(strcmp(S{ii}.status,'open'))
    fclose(S{ii});
    S{ii}=0;
    set(handles.notes,'String','Serial Port close','ForegroundColor',[0 0 0]);  
    if(ii==1)
        set(handles.EnableLeft,'value',0);
    else
        set(handles.EnableRight,'value',0);
    end
else
    set(handles.notes,'String','Serial Port already close!','ForegroundColor',[1 0 0]);    
end