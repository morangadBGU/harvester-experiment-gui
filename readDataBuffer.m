function [ Data ] = readDataBuffer( BuffData)
%READBUFFER Reads Data from serial buffer and translates 2 bytes to int16
    
    Data.Pot=Bytes2Int16(BuffData(1:2)); 
    Data.Hall=Bytes2Int16(BuffData(3:4));
%     Data.Hall=BuffData(4);
%     Data.PR=Bytes2Int16(BuffData(3:4));
    Data.HarvestT=Bytes2Int16(BuffData(5:6));  
    Data.Vin=Bytes2Int16(BuffData(7:8)); 
    Data.Vout=Bytes2Int16(BuffData(9:10)); 
    Data.PR=Bytes2Int16(BuffData(11:12));
%     Data.IndexInSeg=BuffData(11); % in current segment
%     Data.SegAVGlength=BuffData(12); % off current segment
    Data.K=BuffData(13); %K value [1-4]
    Data.Iref=Bytes2Int16(BuffData(14:15)); % feedback from harvster on last Iref command
    Data.time=Bytes2Int32(BuffData(16:19)); %timstamp from PIC [ms]
    Data.SegUpdate=BuffData(20); % update  counter from PIC
end

