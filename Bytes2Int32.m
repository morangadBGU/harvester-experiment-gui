function [ intOut ] = Bytes2Int32( bytesArr )
a=bitshift(bytesArr(1),24);
b=bitshift(bytesArr(2),16);
c=bitshift(bytesArr(3),8);
d=bytesArr(4);

intOut=bitor(a,bitor(b,bitor(c,d)));
% intOut=bitor(a,b);
intOut=double(typecast(uint32(intOut),'int32'));
end

