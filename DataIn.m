function DataIn( s,event )    
% Function DataIn called with every complete data is in serial buffer
% The function trnsaltes the data, calculates the parameters for control
% and send the desired current to harvester.
    global Th PacketSize LeftPort 
    global ghandles %Data DataVect
    global Tk Counter RecCounter RecData %StTime
    global Resistor dt%ToUse
    global PlotBuffFull PlotBuffIndex PlotBuffer PlotBuffLength
    global ProfileVersion fTest timeOld 
    global a b
    
    
%% defining Source
    if(strcmp(s.port,LeftPort))
        axesHandle=ghandles.LeftAxes;
        SideChar='L';
        SideNum=1;
    else
        axesHandle=ghandles.RightAxes;
        SideChar='R';
        SideNum=2;
    end    
%% expacting $ sign
    while(fread(s,1)~='$')        
        disp(Counter(SideNum));
    end    
%% Reading buffer
    Counter(SideNum)=Counter(SideNum)+1;
    BuffData=fread(s,PacketSize-1);

% if mod(Counter(SideNum),2) %enters every even sample   
%% analysing buffer
    Data=readDataBuffer(BuffData);
    DecodeMeasuredData();      

    if(~get(ghandles.pause,'value'))            
        PlotBuffIndex(SideNum)=PlotBuffIndex(SideNum)+1;
        PlotBuffer{SideNum}(PlotBuffIndex(SideNum),1:9)=[Data.time Data.Pot Data.PR Data.HarvestT...
                           Data.Vin Data.Vout Data.K Data.Iref Data.Hall];
        if(PlotBuffIndex(SideNum)>=PlotBuffLength )  
            PlotData();
            PlotBuffIndex(SideNum)=0;
            PlotBuffer{SideNum}=zeros(PlotBuffLength,9);
        end
        % saves data to memory    
        if(get(ghandles.Record,'value'))&&(RecCounter(SideNum)<60000)
           RecCounter(SideNum)=RecCounter(SideNum)+1;
           RecData{SideNum}(RecCounter(SideNum),:)=[Data.time Data.Pot Data.PR Data.HarvestT...
               Data.Vin Data.Vout Data.K Data.Iref];%Data.SegPR];
       end
    end

% end

end

     



