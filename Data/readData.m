clc;clear all;%close all;
set(0,'DefaultFigureWindowStyle','docked');

Name='Ori_1.84_85_27_40_15%_17_03_16_11_25_18_Right.mat';
%% define parameters
load(Name)
choice=1;
onecicle=0;
% onecicle=[7250 7472];
lim(1)=length(SaveData(:,2));   %all data
ang=(SaveData(1:lim(choice),2));
K=SaveData(1:lim(choice),7);
iref=SaveData(1:lim(choice),8);
ipic=SaveData(1:lim(choice),4);
n=1:lim(choice);
Vout=SaveData(1:lim(choice),6);
Vin=SaveData(1:lim(choice),5);

%% cleaning data
ipic(ipic>30)=0; 
iref(iref>30)=0;
% ang(1322)=ang(1323);

%% plotting
h1=subplot(2,1,1);
ha = plotyy(n,K,n,ang,'plot');
set(ha(1),'ylim',[0.9 4.1]);
set(ha(2),'ylim',[min(ang)-2 min(ang)+70]);
xlabel('Sampels');
ylabel(ha(1),'V segment');
ylabel(ha(2),'Raw Data (angle)');


if (onecicle)
    h2=subplot(2,1,2);
    hold on
    title(h2,'Zoom In');
    n2=n(onecicle(1):onecicle(2));
    K2=K(onecicle(1):onecicle(2));
    ang2=ang(onecicle(1):onecicle(2));
    ha = plotyy(n2,K2,n2,ang2,'plot');
    set(ha(1),'ylim',[0.9 4.1]);
    set(ha(2),'ylim',[min(ang)-2 min(ang)+70]);
    xlabel('Sampels');
    ylabel(ha(1),'V segment');
    ylabel(ha(2),'Raw Data (angle)');
end

if (length(Name)>22)
    onecicle=[27 330];
    figure
    h3=subplot(2,1,1);
    plot(n,iref,n,ipic,'r')
    hold on
    xlabel('Sampels')
    ylabel('current')
    legend('i ref','i pic')
    if(onecicle)
        h4=subplot(2,1,2);
        hold on
        title(h4,'Zoom In');
        plot(n(onecicle(1):onecicle(2)),iref(onecicle(1):onecicle(2)),...
        n(onecicle(1):onecicle(2)),ipic(onecicle(1):onecicle(2)),'r');
        hold on
        axis(h4,[onecicle -0.1 10]); 
    end
end

figure
h3=subplot(2,1,1)
ha = plotyy(n,K,n,ang,'plot');
set(ha(1),'ylim',[0.9 4.1]);
set(ha(2),'ylim',[min(ang)-2 min(ang)+70]);
xlabel('Sampels');
ylabel(ha(1),'V segment');
ylabel(ha(2),'Raw Data (angle)');

h4=subplot(2,1,2);
% plot(n,iref,n,ipic,'r')
% hold on
% xlabel('Sampels')
% % ylabel('current')
% legend('i ref','i pic')
% linkaxes([h1 h2],'x');
% plot(n,Vin,'k')
ha2 = plotyy(n,iref,n,Vin,'plot');
hold on
plot(n,ipic,'r')
set(ha2(1),'ylim',[0 20]);
% set(ha,'xlim',[60 300]);
% set(h3,'xlim',[60 300]);
% set(h4,'xlim',[60 300]);
linkaxes([h3 h4 ha2],'x');
legend('i ref','i pic','V in')
ylabel(ha2(1),'Current');
ylabel(ha2(2),'Voltage');


figure
h3=subplot(4,1,1)
ha = plotyy(n,K,n,ang,'plot');
set(ha(1),'ylim',[0.9 4.1]);
set(ha(2),'ylim',[min(ang)-2 min(ang)+70]);
xlabel('Sampels');
ylabel(ha(1),'V segment');
ylabel(ha(2),'Raw Data (angle)');

h4=subplot(4,1,2);
% plot(n,iref,n,ipic,'r')
% hold on
% xlabel('Sampels')
% % ylabel('current')
% legend('i ref','i pic')
% linkaxes([h1 h2],'x');
% plot(n,Vin,'k')
ha2 = plotyy(n,iref,n,Vin,'plot');
hold on
plot(n,ipic,'r')
set(ha2(1),'ylim',[0 20]);
set(ha,'xlim',[60 300]);
set(h3,'xlim',[60 300]);
set(h4,'xlim',[60 300]);
linkaxes([h3 h4 ha2],'x');
legend('i ref','i pic','V in')
ylabel(ha2(1),'Current');
ylabel(ha2(2),'Voltage');



% load('C:\Users\user\Google Drive\reaserch\knee\kneeexp\dataandanalysis - Simulation\North carolina data\NCsu_levelWalk.mat'); %col 1: angle; col 2: moment ;col 3: power
% angle=WalkLevel(:,1);
% moment=WalkLevel(:,2);
% power=WalkLevel(:,3);
% samples=(1:length(angle))';
% 
% [val(1),ind(1)]=max(angle(1:30));
% [val(2),ind(2)]=min(angle(30:50));  ind(2)=29+ind(2);
% [val(3),ind(3)]=max(angle(50:80));  ind(3)=49+ind(3);
% [val(4),ind(4)]=min(angle(80:100)); ind(4)=79+ind(4);
% 
% gait_freq=1; %Hz
% dn=angle(2)-angle(1);
% dt=1/100;
% 
% subplot(4,1,3)
% plot(samples,angle);
% hold on
% plot([13.5 13.5],[0 80],'r');
% plot([ind(2) ind(2)],[0 80],'r');
% plot([ind(3) ind(3)],[0 80],'r');
% plot([ind(4) ind(4)],[0 80],'r');
% 
% ylabel('Angle [deg]');
% xlabel('(a)');
% title('Angle,Power and moment in Normal Walk')
% 
% 
% subplot(4,1,4);
% [x,mom,pow]=plotyy(samples,moment,samples,power);
% xlabel(['Samples No',sprintf('\n'),'(b)']);
% ylabel(x(1),'Torque [J/kg]')
% ylabel(x(2),'Power [W/kg]')
% grid on