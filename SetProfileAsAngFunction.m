% close all;

% TkSource1=cell(4,6);
AngK3=interp1(1:length(ang(PK3)),ang(PK3)-ang(PK3(1)),linspace(1,length(ang(PK3)),100)); %normalized pk3 angle
a=AngK3(end)/99; x=0:99;
y=a*x;
d=AngK3-y; % the difrence between the linear line and the angular profile
B=round(d);
A=1:100;
C3=A+B; % the corrected indexing with the non linear compensation

AngK4=interp1(1:length(ang(PK4)),ang(PK4)-ang(PK4(1)),linspace(1,length(ang(PK4)),100)); %normalized pk3 angle
a=AngK4(end)/99; x=0:99;
y=a*x;
d=AngK4-y;
B=round(d);
C4=A+B;

SampleRate=100;
[B,A]=butter(4,10/(SampleRate/2));  %(cut off freq)/((Sample rate)/2)

for n=1:6
    TkSource1{3,n}=filtfilt(B,A,TkSource{3,n});%(C3(1:end)));
    TkSource1{4,n}=filtfilt(B,A,TkSource{4,n});%(C4(1:end)));
    TkSource{3,n}=TkSource{3,n}(C3(1:end));
    TkSource{4,n}=TkSource{4,n}(C4(1:end));

end

%%%%%%%%%%%%%%%%%%%- moran's plot -%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure();
% plot(y)
% hold on;
% plot(AngK4,'--b')
% plot(d,'g');
% legend('Linear function', ' Angular profile', '(Angular profile) - (Linear function)');
% xlabel('Segment time [%]'); ylabel('angle [deg]');
% 
% figure();
% [ax,h1,h2]=plotyy(0:99,TkSource1{4,5},0:99,d); hold on;
% plot(ax(1),0:99,TkSource{4,5},'r');
% ylabel(ax(1),'(Profile torque) / (Human weight) [Nm/Kg]');
% ylabel(ax(2),'[deg]'); xlabel('Segment time [%]');
% legend('Original normelized Profile torque', ' Linear fit normelized Profile torque', '(Angular profile) - (Linear function)');
% 

