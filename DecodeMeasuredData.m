global HallBiasflag HallBias % triggerd by user
if(SideNum==1) % Left
    uint2PotDeg=a.Left; potUintBias=b.Left; % transfer from [uint] to [V] to [deg]
else %Right
%     uint2PotDeg=1; potUintBias=0;
    uint2PotDeg=a.Right; potUintBias=b.Right; % transfer from [uint] to [V] to [deg]
end
Data.Pot=(Data.Pot-potUintBias)*uint2PotDeg; %[ang] !!!!!!!!!!!!!!!!!!!!!

% if(HallBiasflag) % Biasing Hall to pot angle on user command
%     HallBias=Data.Pot-Data.Hall;
%     HallBiasflag=0;
% end
% uint2HallDeg=-1*(60/(243*2)); % transfer from [uint] to [ticks] to [deg]   
% Data.Hall=(Data.Hall)*uint2HallDeg+HallBias; %[deg] Biased

uint2Torque=1/25;% transfer from [uint] to [A] and then to [Nm]
HarvestTBias=-500;
Data.HarvestT=(Data.HarvestT+HarvestTBias)*uint2Torque; %[Nm]

IrefBias=-500;
Data.Iref=(Data.Iref+IrefBias)*uint2Torque;

uint2Vin=0.03867; % transfer from [uint] to [Vin] %12*3.3/1024
uint2Vout=0.0464; % transfer from [uint] to [Vin] %14.4*3.3/1024
Data.Vin=Data.Vin*uint2Vin;
Data.Vout=Data.Vout*uint2Vout;

% Data.SegPR=round((Data.IndexInSeg/Data.SegAVGlength)*100); % current Present of current K
Data.time=Data.time*dt;