global Tk TkSource offset
offset=0;
% offset=-12;
TkSource=cell(4,5);
Tk=cell(4,1);

load('Opti2015.mat'); 

%% splining and saving NEG torque profiles 
for n=1:6
    TkSource{1,n}=zeros(1,100);%interp1(1:length(K1(:,n)),K1(:,n),linspace(1,length(K1),100)); % interp K1 Torque profile to 100 points
    TkSource{2,n}=zeros(1,100);
    TkSource{3,n}=interp1(1:length(K3(:,n)),K3(:,n),linspace(1,length(K3),100)); % interp K3 Torque profile to 100 points
    K3offset=12;
    TkSource{3,n}=[TkSource{3,n}(K3offset+1:end) zeros(1,K3offset)]; % offset!
    TkSource{4,n}=interp1(1:length(K4(:,n)),K4(:,n),linspace(1,length(K4),100)); % interp K4 Torque profile to 100 points
    K4offset=10;
    TkSource{4,n}=[TkSource{4,n}(K4offset+1:end) zeros(1,K4offset)]; % offset!
end



%%
Tk{1}=zeros(1,100);%TkSource{1}*str2double(get(handles.Weight,'string'));
Tk{2}=zeros(1,100); % no harvest on K2
Tk{3}=TkSource{3}*str2double(get(handles.Weight,'string'));
Tk{4}=TkSource{4}*str2double(get(handles.Weight,'string'));

% figure();
% subplot(4,1,1); plot(TkSource{1}); 
% subplot(4,1,2); plot(TkSource{2});
% subplot(4,1,3); plot(TkSource{3});
% subplot(4,1,4); plot(TkSource{4});