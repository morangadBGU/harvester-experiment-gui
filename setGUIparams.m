serialPorts = instrhwinfo('serial');
set(handles.SerialPortS1,'String',['Select Serial Port'; serialPorts.SerialPorts]);
set(handles.SerialPortS2,'String',['Select Serial Port'; serialPorts.SerialPorts]);
% set(handles.SerialPortS1,'Value',4);
% set(handles.SerialPortS2,'Value',3);

set(handles.Name,'String','Name');
set(handles.Hight,'String','Hight');
set(handles.Weight,'String',80);
set(handles.Age,'String','Age');

set(handles.pause,'Value',0);
set(handles.Ang,'Value',1);
set(handles.Vin,'Value',1);
set(handles.Vout,'Value',1);
set(handles.HarvestTorque,'Value',1);
set(handles.TheoDataON,'Value',0);
set(handles.TheoAng,'Value',0);
set(handles.TheoPower,'Value',0);
set(handles.TheoTorque,'Value',0);
set(handles.TheoNeg,'Value',0);
set(handles.CalcDataON,'Value',0);
set(handles.CalcVel,'Value',0);
set(handles.CalcHpower,'Value',0);
set(handles.CalcInertia,'Value',0);
set(handles.Kselector,'Value',5);
set(handles.GenAng,'Value',1);
set(handles.EnableLeft,'Value',0);
set(handles.EnableRight,'Value',0);

% set(handles.LeftSelect,'Value',1);
set(handles.Resistor,'string','100');
Resistor=str2num(get(handles.Resistor,'string'));
% LeftSelect_Callback(hObject, eventdata, handles)
% set(handles.PR,'String','00');
ProfileSelectorStrings=cell(length(HarvestPRArr),1);
for n=1:length(HarvestPRArr)
    ProfileSelectorStrings{n,1}=[num2str(HarvestPRArr(n)*100),'%'];
end
set(handles.PR,'String',ProfileSelectorStrings);
% str2double(get(handles.HarvestTorque)
% set(handles.TorqueHightTxt,'Value',3);
% set(handles.SendCommand,'Value',0);
load('angleCalib.mat')
set(handles.aLeft,'string',num2str(a.Left));
set(handles.bLeft,'string',num2str(b.Left));
set(handles.aRight,'string',num2str(a.Right));
set(handles.bRight,'string',num2str(b.Right));